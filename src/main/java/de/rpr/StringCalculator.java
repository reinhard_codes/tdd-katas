package de.rpr;

import java.util.HashSet;
import java.util.Set;

public class StringCalculator {

    public static final String DEFAULT_DELIMITERS = ",|\\n";
    private final Set<Integer> invalidInputs = new HashSet<Integer>();

    public int add(String input) {
        if (input == null || input.isEmpty()) {
            return 0;
        }
        String delimiters = getDelimiters(input);
        String numbers = getNumbers(input);
        String[] parts = numbers.split(delimiters);
        int sum = doAdd(parts, 0);
        if (!invalidInputs.isEmpty()) {
            throw new IllegalArgumentException("Invalid values: " + invalidInputs.toString());
        }
        return sum;
    }

    private int doAdd(String[] input, int index) {
        if (index >= input.length) {
            return 0;
        }
        Integer value = Integer.valueOf(input[index]);
        if (value < 0) {
            invalidInputs.add(value);
        }
        if (value > 1000) {
            return doAdd(input, index + 1);
        }
        return value + doAdd(input, index + 1);
    }

    private static String getNumbers(String input) {
        return input.substring(getNumbersStart(input));
    }

    private static String getDelimiters(String input) {
        if (hasCustomDelimiter(input)) {
            return input.substring(2, getDelimiterLineEnd(input));
        }
        return DEFAULT_DELIMITERS;
    }

    private static boolean hasCustomDelimiter(String input) {
        return input.startsWith("//");
    }

    private static int getNumbersStart(String input) {
        if (hasCustomDelimiter(input)) {
            return input.indexOf("\n") + 1;
        }
        return 0;
    }

    private static int getDelimiterLineEnd(String input) {
        return input.indexOf("\n");
    }

}
