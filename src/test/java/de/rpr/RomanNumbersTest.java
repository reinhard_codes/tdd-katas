package de.rpr;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class RomanNumbersTest {

    @Test
    public void one() {
        assertEquals("I", RomanNumbers.convert(1));
    }

    @Test
    public void two() {
        assertEquals("II", RomanNumbers.convert(2));
    }

    @Test
    public void five() {
        assertEquals("V", RomanNumbers.convert(5));
    }

    @Test
    public void six() {
        assertEquals("VI", RomanNumbers.convert(6));
    }

    @Test
    public void ten() {
        assertEquals("X", RomanNumbers.convert(10));
    }

    @Test
    public void twenty() {
        assertEquals("XX", RomanNumbers.convert(20));
    }

    @Test
    public void fifty() {
        assertEquals("L", RomanNumbers.convert(50));
    }

    @Test
    public void hundred() {
        assertEquals("C", RomanNumbers.convert(100));
    }

    @Test
    public void four() {
        assertEquals("IV", RomanNumbers.convert(4));
    }

    @Test
    public void nine() {
        assertEquals("IX", RomanNumbers.convert(9));
    }

    @Test
    public void nineteen() {
        assertEquals("XIX", RomanNumbers.convert(19));
    }

    @Test
    public void fourty() {
        assertEquals("XL", RomanNumbers.convert(40));
    }

    @Test
    public void ninetyNine() {
        assertEquals("XCIX", RomanNumbers.convert(99));
    }

    @Test
    public void zero() {
        assertEquals("", RomanNumbers.convert(0));
    }

}
